<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        DB::table('users')->insert([[
            'name' => 'admin1',
            'email' => 'admin1@gmail.com',
            'type' => 1,
            'password' => Hash::make('admin123'),
        ]
        ,[
            'name' => 'moderator1',
            'email' => 'moderator1@gmail.com',
            'type' => 2,
            'password' => Hash::make('mod123'),
        ]]);
    }
}
