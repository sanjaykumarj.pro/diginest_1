<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\PostController;

Route::post('login', [ApiController::class, 'authenticate']);
Route::post('register', [ApiController::class, 'register']);

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('logout', [ApiController::class, 'logout']);
    Route::get('get_user', [ApiController::class, 'get_user']);
    Route::get('products', [PostController::class, 'index']);
    Route::get('products/{id}', [PostController::class, 'show']);
    Route::post('create', [PostController::class, 'store']);
    Route::put('update/{product}',  [PostController::class, 'update']);
    Route::delete('delete/{product}',  [PostController::class, 'destroy']);
});